# util.views.auth

from flask import redirect, url_for
from app.models import Account
import re

# Email validator
EMAIL_REGEX = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")

# Auth tools
def verify_login(session):
    """
    Validates the user is logged in, otherwise redirects to
    the login page.
    """
    if 'email' not in session:
          return redirect(url_for('login')), None
    else:
        account = Account.objects.get_or_404(email=session['email'])
        return None, account

def verify_email(email):
    """
    Validates an input string matches the email template.
    """
    return EMAIL_REGEX.match(email)
