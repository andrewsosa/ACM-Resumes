# views.nav.nav

from flask_nav import Nav
from flask_nav.elements import *

from app import app

nav_logged_in = Navbar('',
    Link('Log Out','/logout'),
)

nav_logged_out = Navbar('',
    Link('Log In', '/login'),
)

# nav_admin = Navbar('',
#     Link('Home', '/'),
# )

nav = Nav()
nav.register_element('logged_in', nav_logged_in)
nav.register_element('logged_out', nav_logged_out)
# nav.register_element('admin', nav_admin)
nav.init_app(app)
